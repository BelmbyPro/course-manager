# Courses Manager
courses manager est le microservice responsable du cycle de vie d’une course et de ses partants
(chevaux) au sein de PMU.

* Une course a lieu un jour donné et possède un nom et un numéro unique pour ce jour
* Une course possède au moins 3 partants ;
* Chaque partant possède un nom et un numéro ;
* Les partants d’une course sont numérotés à partir de 1, sans doublon ni trou.



# Architecture Globale
* Application Spring Boot en Java
* Base de données SQL
* Bus Kafka

![archi](images/archiglobale.png)

# Architecture Logiciel
L'architecture utlisé c'est  [l'Architecture hexagonale.](https://fr.wikipedia.org/wiki/Architecture_hexagonale)
L'architecture hexagonale, ou architecture à base de ports et d'adaptateurs, est un patron d'architecture utilisé dans le domaine de la conception des logiciels. Elle vise à créer des systèmes à base de composants d'application qui sont faiblement couplés et qui peuvent être facilement connectés à leur environnement logiciel au moyen de ports et d'adaptateurs. Ces composants sont modulaires et interchangeables ce qui renforce la cohérence des traitements et facilite l'automatisation des tests

![archihexa](images/archi_hexa_06-1024x526.png)


### Pre requites
* Java JDK (1.8.151 minimum) 
* IntelliJ Ultimate (conseillé)
* Maven (3.6.1 minimum)

### Run Application 
* La class main est src.main.java.pmu.fr.coursesManager.CoursesManagerApplication
* L'application se lance sur le port 8086

### URLs

* Accès à la documentation du microservice http://localhost:8086/swagger-ui/index.html

![swagger](images/swagger.png)

* Base de données H2 http://localhost:8086/h2-console
  * Driver Class : **org.h2.Driver**
  * JDBC URL : **jdbc:h2:mem:courses-manager-db**
  * User Name : sa
  * Password:
   
![h2](images/h2.png)

### EndPoints

POST http://localhost:8086/races avec body suivant :
```json
{
"name": "string",
"number": "string",
"date": "2022-12-06",
"horses": [
   {
     "name": "string",
     "number": 0
   },
   {
     "name": "string",
     "number": 0
   },
   {
     "name": "string",
     "number": 0
   }
 ]
}
```
> **Warning**
> Rassurez vous que le serveur KAFKA est démarré pour envoyer des messages sur le bus
> Sinon mettez en commentaire le code qui envoie les messages sur le bus kafka pour vos tests
> dans src.main.java.pmu.fr.coursesManager.application.controllers.RaceController

![kafka](images/kafka.png)