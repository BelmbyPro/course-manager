package pmu.fr.coursesmanager.domain.validations;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import pmu.fr.coursesmanager.domain.exceptions.BusinessException;
import pmu.fr.coursesmanager.domain.models.Horse;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;

@SpringBootTest
public class RaceValidationTests {

    @Test
    public void testIsValidHorses_withoutFirstHorseName_shouldThrownBusinessExceptions_withMessage_nameAndNumberOfEachHorseIsMandatory(){
        try {
            List<Horse> horses = Arrays.asList(Horse.builder().number(3).build(),
                    Horse.builder().name("Horse1").number(2).build(),
                    Horse.builder().name("Horse3").number(1).build());
            RaceValidation.isValidHorses(horses);
            fail();
        } catch (BusinessException e) {
            assertThat(e.getMessage()).isEqualTo("name and number of each horse is mandatory");
        }
    }

    @Test
    public void testIsValidHorses_withoutLastHorseNumber_shouldThrownBusinessExceptions_withMessage_nameAndNumberOfEachHorseIsMandatory(){
        try {
            List<Horse> horses = Arrays.asList(Horse.builder().name("Horse1").number(3).build(),
                    Horse.builder().name("Horse1").number(1).build(),
                    Horse.builder().name("Horse3").build());
            RaceValidation.isValidHorses(horses);
            fail();
        } catch (BusinessException e) {
            assertThat(e.getMessage()).isEqualTo("name and number of each horse is mandatory");
        }
    }

    @Test
    public void testIsValidHorses_shouldThrownBusinessExceptions_withMessage_TwoHorsesMustNotHaveTheSameNumber(){
        try {
            List<Horse> horses = Arrays.asList(Horse.builder().name("Horse1").number(3).build(),
                    Horse.builder().name("Horse1").number(1).build(),
                    Horse.builder().name("Horse3").number(1).build());
            RaceValidation.isValidHorses(horses);
            fail();
        } catch (BusinessException e) {
            assertThat(e.getMessage()).isEqualTo("Two horses must not have the same number");
        }
    }

    @Test
    public void testIsValidHorses_shouldThrownBusinessExceptions_withMessage_TheHorseNumberShouldBeBetween1and3(){
        try {
            List<Horse> horses = Arrays.asList(Horse.builder().name("Horse1").number(3).build(),
                    Horse.builder().name("Horse1").number(1).build(),
                    Horse.builder().name("Horse3").number(4).build());
            RaceValidation.isValidHorses(horses);
            fail();
        } catch (BusinessException e) {
            assertThat(e.getMessage()).isEqualTo("The horse number should be between 1 and 3");
        }
    }

    @Test
    public void testIsValidHorses_shouldReturnTrue(){

            List<Horse> horses = Arrays.asList(Horse.builder().name("Horse1").number(3).build(),
                    Horse.builder().name("Horse1").number(1).build(),
                    Horse.builder().name("Horse3").number(2).build());
            RaceValidation.isValidHorses(horses);
            assertThat(RaceValidation.isValidHorses(horses)).isTrue();
    }
}
