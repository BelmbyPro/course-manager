package pmu.fr.coursesmanager.infrastructure.repositories;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import pmu.fr.coursesmanager.infrastructure.entities.RaceEntity;

import java.time.LocalDate;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RaceRepositoryTests {

    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private RaceRepository raceRepository;

    @Before
    public void init() {
        RaceEntity raceEntity = RaceEntity.builder().name("Race1").number("1A").date(LocalDate.parse("2022-12-06")).build();
        entityManager.persist(raceEntity);
    }

    @Test
    public void testFindByDateAndNameAndNumber_isPresent() {
        Optional<RaceEntity> raceEntity = raceRepository.findByDateAndNameAndNumber(LocalDate.parse("2022-12-06"), "Race1", "1A");
        assertThat(raceEntity).isPresent();

    }

    @Test
    public void testFindByDateAndNameAndNumber_isNotPresent() {
        Optional<RaceEntity> raceEntity = raceRepository.findByDateAndNameAndNumber(LocalDate.parse("2022-12-25"), "Race1", "1A");
        assertThat(raceEntity).isNotPresent();
    }

    @Test
    public void testSave() {
        RaceEntity raceEntity = raceRepository.save(RaceEntity.builder().name("Race2").date(LocalDate.parse("2022-12-16")).number("1B").build());
        assertThat(raceEntity).hasFieldOrPropertyWithValue("name", "Race2");
        assertThat(raceEntity).hasFieldOrPropertyWithValue("number", "1B");
        assertThat(raceEntity).hasFieldOrPropertyWithValue("date", LocalDate.parse("2022-12-16"));
    }
}
