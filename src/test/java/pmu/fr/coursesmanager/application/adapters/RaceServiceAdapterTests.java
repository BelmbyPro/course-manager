package pmu.fr.coursesmanager.application.adapters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import pmu.fr.coursesmanager.domain.exceptions.BusinessException;
import pmu.fr.coursesmanager.domain.models.Race;
import pmu.fr.coursesmanager.domain.ports.api.RaceServicePort;
import pmu.fr.coursesmanager.domain.ports.spi.RacePersistencePort;
import pmu.fr.coursesmanager.infrastructure.adapters.RacePersistenceAdapter;
import pmu.fr.coursesmanager.infrastructure.entities.RaceEntity;
import pmu.fr.coursesmanager.infrastructure.exceptions.MappingException;
import pmu.fr.coursesmanager.infrastructure.repositories.RaceRepository;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.util.Optional;

@RunWith(SpringRunner.class)
public class RaceServiceAdapterTests {
    @TestConfiguration
    static class RaceServiceAdapterTestsContextConfiguration{
        @Bean
        public RacePersistencePort racePersistencePort(){
            return new RacePersistenceAdapter();
        }
        @Bean
        public RaceServicePort raceServicePort(){
            return new RaceServiceAdapter(racePersistencePort());
        }
    }
    @Autowired
    private RaceServicePort raceServicePort;

    @MockBean
    private RaceRepository raceRepository;

    @Before
    public void setUp(){
        RaceEntity raceEntity = RaceEntity.builder().name("Race2").date(LocalDate.parse("2022-12-16")).number("1B").build();
        RaceEntity raceEntity1 = new RaceEntity();
        RaceEntity raceEntity2 = RaceEntity.builder().name("Race1").date(LocalDate.parse("2022-12-16")).number("1A").build();
       Mockito.when(raceRepository.findByDateAndNameAndNumber(raceEntity.getDate(),raceEntity.getName(),raceEntity.getNumber()))
                .thenReturn(Optional.of(raceEntity));
        Mockito.when(raceRepository.save(raceEntity))
                .thenReturn(raceEntity);
        Mockito.when(raceRepository.save(raceEntity1))
                .thenReturn(raceEntity1);
        Mockito.when(raceRepository.save(raceEntity2))
                .thenReturn(raceEntity2);
    }

    @Test
    public void testCreateRace_shouldThrownBusinessException(){
        Race race = Race.builder().name("Race2").date(LocalDate.parse("2022-12-16")).number("1B").build();
        try {
            raceServicePort.createRace(race);
            fail();
        } catch (BusinessException e){
            assertThat(e.getMessage()).isEqualTo("The race with the same name and number already exist for that date");
        }
    }

    @Test
    public void testCreateRace_shouldThrownMappingException(){
        Race race = new Race();
        try {
            raceServicePort.createRace(race);
            fail();
        } catch (MappingException e){
            assertThat(e.getMessage()).isEqualTo("mapping of Race to RaceEntity failed, please contact IT");
        }
    }

    @Test
    public void testCreateRace(){
        Race race = Race.builder().name("Race1").date(LocalDate.parse("2022-12-16")).number("1A").build();
        Race raceSave = raceServicePort.createRace(race);
        assertThat(raceSave.getName()).isEqualTo("Race1");
        assertThat(race.getNumber()).isEqualTo("1A");
        assertThat(race).isEqualTo(raceSave);
    }
}
