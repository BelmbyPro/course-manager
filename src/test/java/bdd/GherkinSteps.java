package bdd;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@ContextConfiguration
@CucumberContextConfiguration
public class GherkinSteps {

    private String addURI;
    private HttpHeaders headers;
    @Autowired
    private RestTemplate restTemplate;

    private ResponseEntity response;

    @Given("i set post race service api endpoint")
    public void setPostEndpoint(){
        addURI = "http://localhost:8086/races";
    }
    @When("Set Headers")
    public void setHeader(){
        headers = new HttpHeaders();
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
    }

    @When("Send a POST Http request")
    public void sendPost(){
        String jsonBody = "{\n" +
                "  \"name\": \"race1\",\n" +
                "  \"number\": \"1A\",\n" +
                "  \"date\": \"2022-12-05\",\n" +
                "  \"horses\": [\n" +
                "    {\n" +
                "      \"name\": \"horse1\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"horse2\",\n" +
                "      \"number\": 2\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"horse3\",\n" +
                "      \"number\": 3\n" +
                "    }\n" +
                "  ]\n" +
                "}";

        HttpEntity<String> entity = new HttpEntity<String>(jsonBody,headers);
        restTemplate = new RestTemplate();
        response = restTemplate.postForEntity(addURI,entity,String.class);
    }

    @Then("valid response")
    public void verifyPostResponse(){
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    ///////TO CONTINUE///////
}
