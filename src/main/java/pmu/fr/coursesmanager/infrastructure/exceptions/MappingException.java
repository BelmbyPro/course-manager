package pmu.fr.coursesmanager.infrastructure.exceptions;

public class MappingException extends RuntimeException{
    public MappingException(String message) {
        super(message);
    }
}
