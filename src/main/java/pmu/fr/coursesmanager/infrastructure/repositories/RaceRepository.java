package pmu.fr.coursesmanager.infrastructure.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pmu.fr.coursesmanager.infrastructure.entities.RaceEntity;

import java.time.LocalDate;
import java.util.Optional;

@Repository
public interface RaceRepository extends JpaRepository<RaceEntity,Long> {
    Optional<RaceEntity> findByDateAndNameAndNumber(LocalDate date, String name, String number);
}
