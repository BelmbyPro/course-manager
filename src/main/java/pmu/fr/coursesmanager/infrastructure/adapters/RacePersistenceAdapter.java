package pmu.fr.coursesmanager.infrastructure.adapters;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pmu.fr.coursesmanager.domain.exceptions.BusinessException;
import pmu.fr.coursesmanager.domain.models.Race;
import pmu.fr.coursesmanager.domain.ports.spi.RacePersistencePort;
import pmu.fr.coursesmanager.infrastructure.entities.RaceEntity;
import pmu.fr.coursesmanager.infrastructure.exceptions.MappingException;
import pmu.fr.coursesmanager.infrastructure.mappers.RaceMapper;
import pmu.fr.coursesmanager.infrastructure.repositories.RaceRepository;

import java.util.Optional;

@Service
public class RacePersistenceAdapter implements RacePersistencePort {

    @Autowired
    private RaceRepository raceRepository;
    @Override
    public Race createRace(Race race) {

        Optional<RaceEntity> raceEntity = raceRepository.findByDateAndNameAndNumber(race.getDate(),race.getName(), race.getNumber());

        if (raceEntity.isPresent()){
            throw new BusinessException("The race with the same name and number already exist for that date");
        }

        RaceEntity raceEntityToSave = RaceMapper.INSTANCE.raceToRaceEntity(race);

        if (raceEntityToSave.getNumber()==null){
            throw new MappingException("mapping of Race to RaceEntity failed, please contact IT");
        }

        RaceEntity raceEntitySave = raceRepository.save(raceEntityToSave);
        return RaceMapper.INSTANCE.raceEntityToRace(raceEntitySave);
    }
}
