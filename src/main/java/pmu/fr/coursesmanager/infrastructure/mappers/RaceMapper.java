package pmu.fr.coursesmanager.infrastructure.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pmu.fr.coursesmanager.domain.models.Race;
import pmu.fr.coursesmanager.infrastructure.entities.RaceEntity;

@Mapper(componentModel = "spring")
public interface RaceMapper {
    RaceMapper INSTANCE = Mappers.getMapper(RaceMapper.class);

    RaceEntity raceToRaceEntity(Race race);
    Race raceEntityToRace(RaceEntity raceEntity);
}
