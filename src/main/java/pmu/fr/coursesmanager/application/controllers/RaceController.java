package pmu.fr.coursesmanager.application.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pmu.fr.coursesmanager.application.services.KafkaSender;
import pmu.fr.coursesmanager.domain.models.Race;
import pmu.fr.coursesmanager.domain.ports.api.RaceServicePort;
import pmu.fr.coursesmanager.domain.validations.RaceValidation;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
@RequestMapping("/races")
@Slf4j
public class RaceController {

    private RaceServicePort raceServicePort;

    private KafkaSender kafkaSender;

    @PostMapping
    ResponseEntity createRace(@Valid @RequestBody Race race) {

        RaceValidation.isValidHorses(race.getHorses());
        raceServicePort.createRace(race);
        log.info("Race with number " + race.getNumber() + " And name " + race.getName() + " is created");

        kafkaSender.send(String.valueOf(race));
        log.info("Successfully send Race with number " + race.getNumber() + " And name " + race.getName() + " to the Kafka Topic courses_manager_in_use_topic");

        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
