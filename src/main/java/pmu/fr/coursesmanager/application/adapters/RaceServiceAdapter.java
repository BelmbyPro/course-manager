package pmu.fr.coursesmanager.application.adapters;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pmu.fr.coursesmanager.domain.models.Race;
import pmu.fr.coursesmanager.domain.ports.api.RaceServicePort;
import pmu.fr.coursesmanager.domain.ports.spi.RacePersistencePort;

@Service
@AllArgsConstructor
public class RaceServiceAdapter implements RaceServicePort {

    RacePersistencePort racePersistencePort;
    @Override
    public Race createRace(Race race) {

        return racePersistencePort.createRace(race);

    }
}