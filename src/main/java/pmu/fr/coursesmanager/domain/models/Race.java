package pmu.fr.coursesmanager.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Race {
    @NotBlank(message = "name is mandatory")
    private String name;
    @NotBlank(message = "number is mandatory")
    private String number;
    @FutureOrPresent
    private LocalDate date;
    @NotNull
    @Size(min = 3)
    private List<Horse> horses = new ArrayList<>();

}