package pmu.fr.coursesmanager.domain.ports.spi;

import pmu.fr.coursesmanager.domain.models.Race;

public interface RacePersistencePort {
    Race createRace(Race race);
}
