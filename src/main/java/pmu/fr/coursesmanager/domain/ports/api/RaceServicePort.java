package pmu.fr.coursesmanager.domain.ports.api;

import pmu.fr.coursesmanager.domain.models.Race;

public interface RaceServicePort {
    Race createRace(Race race);
}

