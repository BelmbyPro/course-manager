package pmu.fr.coursesmanager.domain.validations;

import pmu.fr.coursesmanager.domain.exceptions.BusinessException;
import pmu.fr.coursesmanager.domain.models.Horse;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class RaceValidation {
    public static boolean isValidHorses(List<Horse> horses){
        List<Integer> horsesNumbers = new ArrayList<>();

        for (Horse horse: horses){
            if (horse.getName() == null || horse.getNumber() == null){
                throw new BusinessException("name and number of each horse is mandatory");
            }
            if (horse.getNumber()<1 || horse.getNumber()>horses.size()){
                throw new BusinessException("The horse number should be between 1 and " + horses.size());
            }
            horsesNumbers.add(horse.getNumber());
        }

        Set<Integer> set = new HashSet<>(horsesNumbers);
        if (horsesNumbers.size() != set.size()){
            throw new BusinessException("Two horses must not have the same number");
        }
        return true;
    }
}
